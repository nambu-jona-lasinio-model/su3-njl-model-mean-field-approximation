#ifndef DISTRIBUTION_FUNCTIONS_H
#define DISTRIBUTION_FUNCTIONS_H

double boseDistribution(double , double );

double fermiDistribution(double , double );

double fermiMomentum(double , double );

double heavisideTheta(double );

double puiseuxExpansionTln1plusExpArgOverT(double , double );

#endif
